"use strict";

define(function() {
	return function Dice() {
		this.faces = [];

		this.face_add = function( prob, symbol ){
			this.faces.push( [ prob, symbol ] );
		};

		this.index_of = function( prob ){
			var perc=0.0;

			for(var x=0 ; x<this.faces.length ; ++x){
				if( prob<=perc+this.faces[x][0] ){
					return x;
				}
				perc+=this.faces[x][0];
			}

			return null;
		};

		this.face_of = function( prob ){
			var index=this.index_of(prob);
			console.log("dice(prob): "+prob);
			console.log( "JSON.s..(faces): "+JSON.stringify(this.faces) );
			console.log( "this.faces[index]: "+this.faces[index] );
			console.log( "index: "+index );
			return this.faces[ index ][1];
		};
	};
});

