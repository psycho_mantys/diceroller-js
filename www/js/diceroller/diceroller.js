"use strict";

define(['random'],
function(Random) {
	return function Roller( dices, rules ) {
		this.dices = dices ;
		this.rules = rules ;

		this.random = new Random();

		this.rool_dice=function(dice){
			var gx=this.random.gen(100);
			var r=dice.face_of( gx );
			for(var i=0 ; i<r.length ; ++i){
				if( ( this.rules.conf["reroll"] ) && this.rules.conf.reroll[ r[i] ] ){
					if( this.rules.conf.reroll_on==="end" ){
						reroll_poll.push( this.rules.conf.reroll[ r[i]] );
//						console.log( this.rules.conf.reroll[result[r[i]]] );
					}else if ( this.rules.conf.reroll_on==="middle" ){

					}
				}else{
					if( result[ r[i] ]!==undefined ){
						result[ r[i] ]+=1;
					}else{
						result[ r[i] ]=1;
					}	
				}
			}
			return null;
		};

		this.roll = function(){
			var result={};
			var dice_poll=this.dices;
			var reroll_poll=[];

			do{
				dice_poll=dice_poll.concat(reroll_poll);
				reroll_poll=[];

				console.log( "dice poll:" );
				console.log(JSON.stringify( dice_poll ) );

				for(var x=0 ; x<dice_poll.length ; ++x){
					var gx=this.random.gen(100);
					console.log("random: "+gx);
					console.log("result: "+result);
					var r=dice_poll[x].face_of( gx );
					for(var i=0 ; i<r.length ; ++i){
						if( ( this.rules.conf["reroll"] ) && this.rules.conf.reroll[ r[i] ] ){
							if( this.rules.conf.reroll_on==="end" ){
								reroll_poll.push( this.rules.conf.reroll[ r[i]] );
//								console.log( this.rules.conf.reroll[result[r[i]]] );
							}else if ( this.rules.conf.reroll_on==="middle" ){
								
							}
						}else{
							if( result[ r[i] ]!==undefined ){
								result[ r[i] ]+=1;
							}else{
								result[ r[i] ]=1;
							}	
						}
					}
				}

			}while( reroll_poll.length!==0 );

			return result;
		};
	};
});

