"use strict";

define(function() {
	return function Random( gen_func ) {
		if(gen_func){
			this.gen = gen_func;
		}else{
			this.gen = function(sides){
				return Math.ceil(Math.random()*sides);
			};
		}
	};
});

