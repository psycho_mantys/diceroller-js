"use strict";

require.config({
	"baseUrl": ".",
	"paths": {
		'jquery': 'thirdparty/jquery',
		'Handlebars': 'thirdparty/handlebars',
		'bootstrap': 'thirdparty/bootstrap/dist/js/bootstrap.min',
		'bootstrap-combobox': 'thirdparty/bootstrap-combobox/js/bootstrap-combobox',
		'yaap': 'thirdparty/yaap/yaap/yaap',
		'Backbone': 'thirdparty/backbone',
		'underscore': 'thirdparty/underscore',
		'FileSaver': 'thirdparty/FileSaver',
		'Templates': 'js/epic-rpg-editor/templates',
		'dice': 'js/diceroller/dice',
		'rules': 'js/diceroller/rules',
		'diceroller': 'js/diceroller/diceroller',
		'random': 'js/diceroller/random'
	},
	shim: {
		'jquery': {
			exports: '$'
		},
		'FileSaver': {
			exports: 'saveAs'
		},
		'Handlebars': {
			exports: 'Handlebars'
		},
		'Backbone': {
			exports: 'Backbone',
			deps: ['jquery','underscore']
		},
		'bootstrap': {
			deps: ['jquery']
		},
		'bootstrap-combobox': {
			deps: ['jquery','bootstrap']
		},
		'underscore': {
			exports: '_'
		}
	}
});


require(['Backbone', 'jquery', 'dice', 'rules', 'diceroller', 'bootstrap'],
function(Backbone, $, Dice, Rules, Diceroller ) {
	$( document ).ready( function() {
		$("a[href='"+window.location.hash.substr(1)+"']").addClass("active");

		var AppRouter = Backbone.Router.extend({
			routes: {
				"*actions": "teste" // matches http://example.com/#anything-here
			}
		});
		// Initiate the router
		var app_router = new AppRouter();

		app_router.on('route:teste', function(actions) {
			var dice=new Dice();
			var rules=new Rules();

			var roller=new Diceroller( [dice], rules );

			dice.face_add( 25, ['1']);
			dice.face_add( 25, ['2']);
			dice.face_add( 25, ['3']);
			dice.face_add( 25, ['4']);

			$(".main-view").html( dice.face_of(25) );
			$(".main-view").html( dice.index_of(1) );
			$(".main-view").html( roller.roll() );
//			$(".main-view").html( JSON.stringify(dice) );
		});

		// Start Backbone history a necessary step for bookmarkable URL's
		Backbone.history.start();
	});
});

