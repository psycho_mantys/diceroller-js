"use strict";

require.config({
	"baseUrl": "www/",
	"paths": {
		'Handlebars': 'thirdparty/handlebars',
		'yaap': 'thirdparty/yaap/yaap/yaap',
		'Backbone': 'thirdparty/backbone',
		'underscore': 'thirdparty/underscore',
		'dice': 'js/diceroller/dice',
	},
	shim: {
		'jquery': {
			exports: '$'
		},
		'Handlebars': {
			exports: 'Handlebars'
		},
		'Backbone': {
			exports: 'Backbone',
			deps: ['jquery','underscore']
		},
		'bootstrap': {
			deps: ['jquery']
		},
		'bootstrap-combobox': {
			deps: ['jquery','bootstrap']
		},
		'underscore': {
			exports: '_'
		}
	}
});



define(['dice'],
function(Dice) {

describe("Interface of dice", function() {
	
	it("ctor", function() {
		var dice=new Dice();
		expect( typeof(dice) ).not.toEqual( typeof(undefined) );
	});
	it("add faces", function() {
		var dice=new Dice();

		dice.face_add( 25, ['1']);
		dice.face_add( 25, ['2']);
		dice.face_add( 25, ['3']);
		dice.face_add( 25, ['4']);

		expect( typeof(dice) ).not.toEqual( typeof(undefined) );
	});
	it("get symbols by index", function() {
		var dice=new Dice();

		dice.face_add( 12.5, ['1']);
		dice.face_add( 12.5, ['2']);
		dice.face_add( 12.5, ['3']);
		dice.face_add( 62.5, ['4']);

		expect( dice.index_of(10.0) ).toEqual( 0 );
		expect( dice.index_of(10) ).toEqual( 0 );

		expect( dice.index_of(0) ).toEqual( 0 );
		expect( dice.index_of(0.0) ).toEqual( 0 );

		expect( dice.index_of(100) ).toEqual( 3 );
		expect( dice.index_of(100.0) ).toEqual( 3 );

		expect( dice.index_of(12.5) ).toEqual( 0 );
		expect( dice.index_of(12.25) ).toEqual( 0 );
		expect( dice.index_of(12.75) ).toEqual( 1 );
	});
	it("get symbols by face", function() {
		var dice=new Dice();

		dice.face_add( 12.5, ['1']);
		dice.face_add( 12.5, ['2']);
		dice.face_add( 12.5, ['3']);
		dice.face_add( 62.5, ['4']);

		expect( dice.face_of(10.0) ).toEqual( ['1'] );
		expect( dice.face_of(10) ).toEqual( ['1'] );

		expect( dice.face_of(0) ).toEqual( ['1'] );
		expect( dice.face_of(0.0) ).toEqual( ['1'] );

		expect( dice.face_of(100) ).toEqual( ['4'] );
		expect( dice.face_of(100.0) ).toEqual( ['4'] );

		expect( dice.face_of(12.5) ).toEqual( ['1'] );
		expect( dice.face_of(12.25) ).toEqual( ['1'] );
		expect( dice.face_of(12.75) ).toEqual( ['2'] );
	});
});

});

