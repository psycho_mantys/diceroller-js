"use strict";

require.config({
	"baseUrl": "www/",
	"paths": {
		'jquery': 'thirdparty/jquery',
		'Handlebars': 'thirdparty/handlebars',
		'bootstrap': 'thirdparty/bootstrap/dist/js/bootstrap.min',
		'bootstrap-combobox': 'thirdparty/bootstrap-combobox/js/bootstrap-combobox',
		'yaap': 'thirdparty/yaap/yaap/yaap',
		'Backbone': 'thirdparty/backbone',
		'underscore': 'thirdparty/underscore',
		'FileSaver': 'thirdparty/FileSaver',
		'Templates': 'js/epic-rpg-editor/templates',
		'dice': 'js/diceroller/dice',
		'rules': 'js/diceroller/rules',
		'diceroller': 'js/diceroller/diceroller',
		'random': 'js/diceroller/random'
	},
	shim: {
		'jquery': {
			exports: '$'
		},
		'FileSaver': {
			exports: 'saveAs'
		},
		'Handlebars': {
			exports: 'Handlebars'
		},
		'Backbone': {
			exports: 'Backbone',
			deps: ['jquery','underscore']
		},
		'bootstrap': {
			deps: ['jquery']
		},
		'bootstrap-combobox': {
			deps: ['jquery','bootstrap']
		},
		'underscore': {
			exports: '_'
		}
	}
});



define(['Backbone', 'jquery', 'dice', 'rules', 'diceroller', 'random'],
function(Backbone, $, Dice, Rules, Diceroller, Random ) {

describe("Interface of game rules", function() {
	it("ctor", function() {
		var rules=new Rules();

		expect(rules).not.toEqual(undefined);
	});
	it("ctor with all parameters", function() {
		var dice=new Dice();

		dice.face_add( 25, ['1']);
		dice.face_add( 25, ['2']);
		dice.face_add( 25, ['3']);
		dice.face_add( 25, ['4']);

		var rules=new Rules({
			reroll: {
				"symbol_1" : dice,
				"symbol_2" : dice
			},
			reroll_on: "end",
			count_reroll: "first"
		});

		expect(rules).not.toEqual(undefined);
	});
});
describe("The change of rules", function() {
	it("simple d4 roll, no reroll", function() {
		var dice=new Dice();
		var rules=new Rules();

		var roller=new Diceroller( [dice], rules );

		var i=0;
		roller.random=new Random(function(){
			var a=[ 10, 10 ];
			//return a[i++];
			return a[i];
		});

		dice.face_add( 25, ['1']);
		dice.face_add( 25, ['2']);
		dice.face_add( 25, ['3']);
		dice.face_add( 25, ['4']);

		var result=roller.roll();

		expect(result["1"]).toEqual(1);
	});
	it("simple d20 roll, 20 reroll", function() {
		var dice=new Dice();

		for(var x=1 ; x<21 ; ++x){
			dice.face_add( 5, [x]);
		}

		var rules=new Rules({
			reroll: {
				20 : dice
			},
			reroll_count: "begin"
			reroll_on: "end"
		});

		var roller=new Diceroller( [dice], rules );

		var i=0;
		roller.random=new Random(function(){
			var a=[ 100, 100, 10, 10, 10 ];
			if( i>2 ) {
				i=2;
			}
			return a[i++];
//			return a[i];
		});

		var result=roller.roll();

		expect(result).toEqual({20:2});
	});
});

});

