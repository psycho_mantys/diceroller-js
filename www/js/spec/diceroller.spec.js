"use strict";

require.config({
	"baseUrl": "www/",
	"paths": {
		'yaap': 'thirdparty/yaap/yaap/yaap',
		'dice': 'js/diceroller/dice',
		'rules': 'js/diceroller/rules',
		'diceroller': 'js/diceroller/diceroller',
		'random': 'js/diceroller/random'
	},
	shim: {
		'underscore': {
			exports: '_'
		}
	}
});



define(['dice', 'rules', 'diceroller', 'random'],
function(Dice, Rules, Diceroller, Random ) {

describe("Interface of diceroller", function() {
	it("ctor", function() {
		var dice=new Dice();
		var rules=new Rules();

		var roller=new Diceroller( [dice], rules );

		expect( typeof(roller) ).not.toEqual( typeof(undefined) );
	});
	it("Roll a dice pool", function() {
		var dice=new Dice();
		var rules=new Rules();

		var roller=new Diceroller( [dice], rules );

		dice.face_add( 25, ['1']);
		dice.face_add( 25, ['2']);
		dice.face_add( 25, ['3']);
		dice.face_add( 25, ['4']);

		var result=roller.roll();

		expect( typeof(result) ).not.toEqual( typeof(undefined) );
		expect( result ).not.toEqual( null );
	});
});
describe("Change the random generator of the diceroller", function() {
	it("Change the random generator function", function() {
		var dice=new Dice();
		var rules=new Rules();

		var roller=new Diceroller( [dice], rules );

		var i=0;
		roller.random=new Random(function(){
			var a=[ 10, 35, 60, 85 ];
			return a[i++];
		});

		dice.face_add( 25, ['1']);
		dice.face_add( 25, ['2']);
		dice.face_add( 25, ['3']);
		dice.face_add( 25, ['4']);

		var result=roller.roll();
		expect(result).toEqual({'1':1});

		result=roller.roll();
		expect(result).toEqual({'2':1});

		result=roller.roll();
		expect(result).toEqual({'3':1});

		result=roller.roll();
		expect(result).toEqual({'4':1});
	});
});

});

